from django.contrib import admin
from . import models

admin.site.register(models.SlistCategory)
admin.site.register(models.SlistLocation)
admin.site.register(models.SlistList)
admin.site.register(models.SlistItem)
