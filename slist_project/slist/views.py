from django.shortcuts import render, redirect, get_object_or_404
from .models import SlistCategory, SlistLocation, SlistList, SlistItem
# Bei from . import models muss models. vor die Model Namen geschrieben werden


def index(request):
    list_all = SlistList.objects.all()
    location_all = SlistLocation.objects.all()
    category_all = SlistCategory.objects.all()
    context = {"list_all": list_all, "category_all": category_all, "location_all": location_all}
    return render(request, "slist/index.html", context)


def create_list(request):
    name = request.POST["list_name"]  # name -> object in SlistList, "name" -> <input name="list_name">
    description = request.POST["list_description"]
    category = SlistCategory.objects.get(pk=request.POST["list_category"])
    location = SlistLocation.objects.get(pk=request.POST["list_location"])

    SlistList.objects.create(name=name, description=description, category=category, location=location)
    # Kein status, da default vorhanden; kein total_price, da optional
    # Alternativ: models.SlistList.objects.create(name=request.POST["list_name"], ...)
    return redirect("index")


def list_detail(request, list_detail_id):
    context = {}
    context["list_all"] = SlistList.objects.all()
    context["location_all"] = SlistLocation.objects.all()
    context["detailed_list"] = get_object_or_404(SlistList, pk=list_detail_id)
    context["item_all"] = SlistItem.objects.filter(list_name=list_detail_id)  # Alle Einträge, deren list_name mit der ausgewählten Liste angehören, werden angezeigt

    return render(request, "slist/list_detail.html", context,)


def create_item(request):
    name = request.POST["item_name"]
    description = request.POST["item_description"]
    list_name = SlistList.objects.get(pk=request.POST["item_list_name"])
    location = SlistLocation.objects.get(pk=request.POST["item_location"])
    amount = request.POST["item_amount"]
    price = request.POST["item_price"]
    # Kein status, da default vorhanden

    SlistItem.objects.create(name=name, description=description, list_name=list_name, location=location, amount=amount, price=price)
    return redirect("index")


def location(request, location_id):
    pass


