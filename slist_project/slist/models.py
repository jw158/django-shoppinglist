from django.db import models


# Model muss definiert werden, bevor es in einem ForeignKey referenziert wird. Ansonsten "NameError: name 'MODEL' is not defined" bei makemigrations


class SlistCategory(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)  # optional

    def __str__(self):
        return self.name


class SlistLocation(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)  # optional

    def __str__(self):  # Damit der Name in der Admin-Oberfläche angezeigt wird
        return self.name


class SlistList(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)  # optional
    status = models.BooleanField(default=False)
    category = models.ForeignKey(to=SlistCategory, on_delete=models.CASCADE, null=True)  # Beim Löschen der Kategorie werden alle zugehörigen Listen gelöscht; null=True, da erst später hinzugefügt
    location = models.ForeignKey(to=SlistLocation, on_delete=models.SET_NULL, null=True, blank=True)
    price_total = models.FloatField(null=True, blank=True)  # optional

    def __str__(self):
        return self.name


class SlistItem(models.Model):
    name = models.TextField()
    description = models.TextField(null=True, blank=True)  # optional
    status = models.BooleanField(default=False)
    list_name = models.ForeignKey(to=SlistList, on_delete=models.CASCADE)  # Wird der Ort gelöscht, sind die Listen und Listeneinträge nicht mehr damit verknüpft
    location = models.ForeignKey(to=SlistLocation, on_delete=models.SET_NULL, null=True, blank=True)  # Wird eine Liste gelöscht, werden auch alle enthaltenen Einträge gelöscht
    amount = models.IntegerField(null=True, blank=True)  # optional
    price = models.FloatField(null=True, blank=True)  # optional

    def __str__(self):
        return self.name

